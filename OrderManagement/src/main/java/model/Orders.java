package model;

import dataAccessLayer.ClientDAO;
import dataAccessLayer.ProductDAO;

/**
 * @author Denisa Popa
 * <p>
 * Orders : resembles orders table from database
 * Extends BasicModel for id
 */
public class Orders extends BasicModel {
    private int clientId;
    private int productId;
    private int quantity;

    /**
     * @param idOrder  order identifier
     * @param clientId client which order
     * @param productId ordered product identifier
     * @param quantity  amount of product ordered
     */

    public Orders(Integer idOrder, Integer clientId, Integer productId, Integer quantity) { //changed from int to Integer to be able to use at reflectance
        super(idOrder);
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Orders(Integer clientId, Integer productId, Integer quantity) {
        super();
        this.clientId = clientId;
        this.productId = productId;
        this.quantity = quantity;
    }

    public Orders(String clientName, String productname, Integer quantity) {
        super();
        this.clientId = ClientDAO.findByName(clientName).getId();
        this.productId = ProductDAO.findByName(productname).getId();
        this.quantity = quantity;
    }

    public int getClientId() {
        return clientId;
    }

    public void setClientId(int clientId) {
        this.clientId = clientId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Order{" +
                "clientId=" + clientId +
                ", productId=" + productId +
                ", quantity=" + quantity +
                '}';
    }
}
