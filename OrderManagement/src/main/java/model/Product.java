package model;

/**
 * @author Denisa Popa
 * <p>
 * Product : resembles product table from database
 * Extends BasicModel for id
 */
public class Product extends BasicModel {
    private String nameProduct;
    private int quantity;
    private float price;

    /**
     * @param idProduct   product identifier
     * @param nameProduct  product name
     * @param quantity   stock of this product
     * @param price     price of product
     */
    public Product(int idProduct, String nameProduct, Integer quantity, Float price) {
        super(idProduct);
        this.nameProduct = nameProduct;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(String nameProduct, Integer quantity, Float price) {
        super();
        this.nameProduct = nameProduct;
        this.price = price;
        this.quantity = quantity;
    }

    public Product(String nameProduct) {
        super(null);
        this.nameProduct = nameProduct;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "Product{" +
                "nameProduct='" + nameProduct + '\'' +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
