package model;

/**
 * @author Denisa Popa
 * <p>
 * Client resembles the client table from the database
 * Extends BasicModel for id
 */
public class Client extends BasicModel {
    private String name;
    private String address;

    /**
     * @param id identifier
     * @param name  client name
     * @param address address where the client lives
     */
    public Client(int id, String name, String address) {
        super(id);
        this.name = name;
        this.address = address;
    }

    public Client(String name, String address) {
        super();
        this.name = name;
        this.address = address;
    }

    public Client(String name) {
        super(null);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Client{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                '}';
    }
}
