package model;

/**
 * @author Denisa Popa
 * Universal class for get/set  identifier
 */
public class BasicModel {
    private Integer id;

    public BasicModel() {
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public BasicModel(Integer id) {
        this.id = id;
    }


}