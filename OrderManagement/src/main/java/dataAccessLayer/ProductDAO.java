package dataAccessLayer;

import connection.ConnectionFactory;
import model.Product;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


/**
 * @author Denisa Popa
 * <p>
 * ProductDAO  CRUD for product
 */

public class ProductDAO extends AbstractDAO {
    private static final Logger LOGGER = Logger.getLogger(Product.class.getName());

    private final static String findStatementString = "SELECT * FROM product where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM product where nameProduct = ?";

    public static List<Product> getProductsList() throws SQLException {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement productStatement = dbConnection.createStatement();
        ResultSet rs = productStatement.executeQuery("SELECT * from product");

        List<Product> productsList = new ArrayList<>();
        while (rs.next()) {
            Product newProduct = new Product(rs.getInt("id"), rs.getString("nameProduct"), rs.getInt("quantity"), rs.getFloat("price"));
            productsList.add(newProduct);
        }
        dbConnection.close();
        return productsList;

    }

    public static Product findById(int id) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, id);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("nameProduct");
            int quantity = rs.getInt("quantity");
            float price = rs.getFloat("price");
            toReturn = new Product(id, name, quantity, price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static Product findByName(String nameProduct) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, nameProduct);
            rs = findStatement.executeQuery();
            rs.next();

            int idProduct = rs.getInt("id");
            int quantity = rs.getInt("quantity");
            float price = rs.getFloat("price");
            toReturn = new Product(idProduct, nameProduct, quantity, price);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:findByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static int insertProduct(Product product) throws NoSuchFieldException {
        Product p = ProductDAO.findByName(product.getNameProduct());// from data base
        if (p == null) {
            return Insert(product); // GENERIC METHOD
        } else {
            int newQuant = product.getQuantity() + p.getQuantity();
            product.setQuantity(newQuant);
            product.setId(p.getId());
            Update(product);
            return product.getId();

        }
    }

    public static void deleteProduct(Product product) {
        Product toDelete = findByName(product.getNameProduct());

        if (toDelete != null) {
            try {
                Delete(toDelete);
            } catch (Exception e) {
                System.out.println("Error at deleting element");
            }
        } else {
            throw new RuntimeException("This product doesn`t exist " + product.toString());
        }

    }

    public static void updateProduct(Product product) throws NoSuchFieldException {
        int pID = product.getId();
        int productID = 0;
        try {
            productID = pID;
        } catch (Exception e) {
            System.out.println("Bad ID!");
        }
        Product myNewProduct = new Product(productID, product.getNameProduct(),
                Integer.parseInt(String.valueOf(product.getQuantity())),
                Float.parseFloat(String.valueOf(product.getPrice())));
        Update(myNewProduct); // GENERIC METHOD
    }
}
