package dataAccessLayer;

import connection.ConnectionFactory;
import model.Client;
import model.Orders;
import model.Product;
import presentation.Bill;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Denisa Popa
 * <p>
 * OrderDAO  CRUD for orders
 */
public class OrderDAO extends AbstractDAO {

    public static List<Orders> getOrderList() throws SQLException {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement orderStatement = dbConnection.createStatement();
        ResultSet rs = orderStatement.executeQuery("SELECT * from orders");

        List<Orders> ordersList = new ArrayList<>();
        while (rs.next()) {
            Orders myNewOrder = new Orders(rs.getInt("id"), rs.getInt("clientID"), rs.getInt("productID"),
                    rs.getInt("quantity"));
            ordersList.add(myNewOrder);
        }
        dbConnection.close();
        return ordersList;
    }

    public static void insertOrder(Orders order) throws Exception {

        int productID = order.getProductId();

        Product myProduct = ProductDAO.findById(productID);
        int productStock = myProduct.getQuantity();
        Client client = ClientDAO.findById(order.getClientId());
        if (order.getQuantity() <= productStock) {
            myProduct.setQuantity(productStock - order.getQuantity());
            Update(myProduct);

            int orderId = Insert(order);
            order.setId(orderId);
            Bill.bill(order, client, myProduct);

        } else {
            System.out.println("Quantity bigger than stock! -> ERROR ");
            Bill.insufficientStockBill(order, client, myProduct);
        }
    }


}
