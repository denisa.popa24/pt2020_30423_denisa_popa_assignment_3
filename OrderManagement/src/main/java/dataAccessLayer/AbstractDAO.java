package dataAccessLayer;

import connection.ConnectionFactory;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @author Denisa Popa
 * Abstract class for CRUD operations
 */
public class AbstractDAO {

    /**
     * @param genericObject CREATE inserts a new entry into the database
     * @return generated id for new entry
     */
    public static int Insert(Object genericObject) {
        String tableName = genericObject.getClass().getSimpleName().toLowerCase();
        StringBuilder query = new StringBuilder();
        StringBuilder values = new StringBuilder();
        query.append("Insert into ").append(tableName).append(" (");
        Field[] allFields = genericObject.getClass().getDeclaredFields();
        try {
            for (int i = 0; i < allFields.length - 1; i++) {
                allFields[i].setAccessible(true);
                query.append(allFields[i].getName());
                query.append(", ");
                Object value = allFields[i].get(genericObject);
                String fieldType = allFields[i].getType().getSimpleName();
                if (fieldType.equals("String"))
                    values.append("\"").append(value).append("\"");
                else
                    values.append(value);
                values.append(", ");
            }
            int lastFieldIndex = allFields.length - 1;
            allFields[lastFieldIndex].setAccessible(true);
            query.append(allFields[lastFieldIndex].getName());
            Object value = allFields[lastFieldIndex].get(genericObject);
            String fieldType = allFields[lastFieldIndex].getType().getSimpleName();
            if (fieldType.equals("String"))
                values.append("\"").append(value).append("\"");
            else
                values.append(value);
            query.append(") values (").append(values).append(" )");
            System.out.println(query);
        } catch (Exception e) {
            System.out.println("Error at inserting");
        }
        int generatedKey = 0;
        try {
            Connection con = ConnectionFactory.getConnection();
            PreparedStatement prepInsertStatement = con.prepareStatement(query.toString(), Statement.RETURN_GENERATED_KEYS);
            prepInsertStatement.executeUpdate();
            ResultSet rs = prepInsertStatement.getGeneratedKeys();

            if (rs.next()) {
                generatedKey = rs.getInt(1);
            }
            con.close();
            prepInsertStatement.close();
            System.out.println("Record with id: " + generatedKey + " created succesfully!");
        } catch (Exception e) {
            System.out.println("Exception when executing insert query");
        }
        return generatedKey;
    }

    /**
     * @param genericObject Delete a record from the database
     * @throws NoSuchFieldException
     */
    public static void Delete(Object genericObject) throws NoSuchFieldException {
        String tableName = genericObject.getClass().getSimpleName().toLowerCase();
        StringBuilder query = new StringBuilder();
        query.append("Delete from ").append(tableName).append(" where id=");
        Field fieldId = genericObject.getClass().getSuperclass().getDeclaredField("id");
        fieldId.setAccessible(true);
        try {
            Object value = fieldId.get(genericObject);
            query.append(value);
        } catch (Exception e) {
            System.out.println("Error at geting id value");
        }
        System.out.println(query);
        try {
            Connection con = ConnectionFactory.getConnection();
            PreparedStatement prepDeleteStatement = con.prepareStatement(query.toString());
            prepDeleteStatement.executeUpdate();
            con.close();
            prepDeleteStatement.close();
            System.out.println("Record deleted succesfully!");
        } catch (Exception e) {
            System.out.println("Exception when executing delete query");
        }
    }

    /**
     * @param genericObject update a record from the database
     * @throws NoSuchFieldException
     */
    public static void Update(Object genericObject) throws NoSuchFieldException {
        String tableName = genericObject.getClass().getSimpleName().toLowerCase();
        StringBuilder query = new StringBuilder();
        query.append("update ").append(tableName).append(" set ");
        Field[] allFields = genericObject.getClass().getDeclaredFields();
        try {
            for (int i = 0; i < allFields.length - 1; i++) {
                allFields[i].setAccessible(true);
                query.append(allFields[i].getName());
                query.append(" = ");
                Object value = allFields[i].get(genericObject);
                String fieldType = allFields[i].getType().getSimpleName();
                if (fieldType.equals("String"))
                    query.append("\"").append(value).append("\"");
                else
                    query.append(value);
                query.append(", ");
            }
        } catch (Exception e) {
            System.out.println("Error at updating");
        }
        int lastFieldIndex = allFields.length - 1;
        allFields[lastFieldIndex].setAccessible(true);
        query.append(allFields[lastFieldIndex].getName());
        query.append(" = ");
        try {
            Object value = allFields[lastFieldIndex].get(genericObject);
            String fieldType = allFields[lastFieldIndex].getType().getSimpleName();
            if (fieldType.equals("String"))
                query.append("\"").append(value).append("\"");
            else
                query.append(value);
        } catch (Exception e) {
            System.out.println("Error at updating");
        }

        query.append(" where id = ");
        Field fieldId = genericObject.getClass().getSuperclass().getDeclaredField("id");
        fieldId.setAccessible(true);

        // query.append(fieldId).append(" = ");

        try {
            Object value = fieldId.get(genericObject);
            query.append(value);
        } catch (Exception e) {
            System.out.println("Error at geting id value");
        }
        try {
            System.out.println(query);
            Connection con = ConnectionFactory.getConnection();
            PreparedStatement prepUpdateStatement = con.prepareStatement(query.toString());
            prepUpdateStatement.executeUpdate();
            con.close();
            prepUpdateStatement.close();
            System.out.println("Record updated succesfully!");
        } catch (Exception e) {
            System.out.println("Exception when executing update query");
        }
    }

}

