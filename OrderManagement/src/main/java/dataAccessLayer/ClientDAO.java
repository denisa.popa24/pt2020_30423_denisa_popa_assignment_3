package dataAccessLayer;

import connection.ConnectionFactory;
import model.Client;

import java.sql.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Denisa Popa
 * <p>
 * ClientDAO  CRUD for client
 */
public class ClientDAO extends AbstractDAO {
    protected static final Logger LOGGER = Logger.getLogger(ClientDAO.class.getName());
    private static final String insertStatementString = "INSERT INTO client(name,address)"
            + " VALUES (?,?)";
    private final static String findStatementString = "SELECT * FROM client where id = ?";
    private final static String findByNameStatementString = "SELECT * FROM client where name = ?";

    public static Client findById(int idClient) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, idClient);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("name");
            String address = rs.getString("address");
            toReturn = new Client(idClient, name, address);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }

    public static Client findByName(String nameClient) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findByNameStatementString);
            findStatement.setString(1, nameClient);
            rs = findStatement.executeQuery();
            rs.next();

            int id = rs.getInt("id");
            String address = rs.getString("address");
            toReturn = new Client(id, nameClient, address);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:findByName " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }


    public static int insert(Client client) {

        return Insert(client);                                                    // GENERIC METHOD
    }

    public static void deleteClient(Client client) {
        Client toDelete = findByName(client.getName());

        if (toDelete != null) {
            try {
                Delete(toDelete);
            } catch (Exception e) {
                System.out.println("Error at deleting element");
            }
        } else {
            throw new RuntimeException("This client doesn`t exist " + client.toString());
        }
    }


    public static void updateClient(Client client) {
        int cID = client.getId();
        String cName = client.getName();
        String cAddress = client.getAddress();
        int clientID = 0;
        clientID = cID;
        try {
            Client myNewClient = searchID(clientID);
            myNewClient.setName(cName);
            myNewClient.setAddress(cAddress);
            Update(myNewClient);                                    // GENERIC METHOD
        } catch (Exception e) {
            System.out.println("Client not found");
        }
    }


    public static List<Client> getClientsList() throws SQLException {
        Connection dbConnection = ConnectionFactory.getConnection();
        Statement clientStatement = dbConnection.createStatement();
        ResultSet rs = clientStatement.executeQuery("SELECT * from client");

        List<Client> clientsList = new ArrayList<>();
        while (rs.next()) {
            Client newClient = new Client(rs.getInt("id"), rs.getString("name"), rs.getString("address"));
            clientsList.add(newClient);
        }
        dbConnection.close();
        return clientsList;

    }


    public static Client searchID(int id) throws SQLException {
        Client myClient = null;

        List<Client> clientsList = getClientsList();
        Iterator<Client> myIterator = clientsList.iterator();
        while (myIterator.hasNext()) {
            Client currentClient = myIterator.next();
            if (currentClient.getId() == id) {
                myClient = currentClient;
                break;
            }
        }
        return myClient;
    }
}
