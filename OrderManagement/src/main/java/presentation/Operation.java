package presentation;

/**
 * @author Denisa Popa
 * An enum for take the commands form file to identify the operations requested by the processed command line
 * This is useful for controller
 */
public enum Operation {

    INSERT_CLIENT("Insert client"),
    DELETE_CLIENT("Delete client"),
    INSERT_PRODUCT("Insert product"),
    DELETE_PRODUCT("Delete product"),
    ORDER("Order"),
    REPORT_CLIENT("Report client"),
    REPORT_ORDER("Report order"),
    REPORT_PRODUCT("Report product");

    private final String text;

    Operation(final String text) {
        this.text = text;
    }

    public static Operation fromString(String text) {
        for (Operation operation : Operation.values()) {
            if (operation.text.equalsIgnoreCase(text)) {
                return operation;
            }
        }
        return null;
    }
}
