package presentation;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import dataAccessLayer.ClientDAO;
import dataAccessLayer.OrderDAO;
import dataAccessLayer.ProductDAO;
import model.Client;
import model.Orders;
import model.Product;

import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Stream;

/**
 * @author Denisa Popa
 * This class contain methods for create pdf with the specific table.
 * Extract rows from database
 */
public class ReportGenerator {

    public static void reportClient() throws IOException, DocumentException, SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_ddHH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("ReportClient" + dtf.format(now) + ".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(3);
        addTableHeaderClient(table);
        List<Client> clients = ClientDAO.getClientsList();
        clients.forEach(c -> {
            table.addCell(String.valueOf(c.getId()));
            table.addCell(c.getName());
            table.addCell(c.getAddress());
        });

        document.add(table);
        document.close();
        System.out.println("Client pdf generated");
    }

    private static void addTableHeaderClient(PdfPTable table) {
        Stream.of("idClient", "Name Client", "Address")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });

    }

    public static void reportProduct() throws IOException, DocumentException, SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_ddHH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("ReportProduct" + dtf.format(now) + ".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(4);
        addTableHeaderProduct(table);

        List<Product> products = ProductDAO.getProductsList();
        products.forEach(p -> {
            table.addCell(String.valueOf(p.getId()));
            table.addCell(p.getNameProduct());
            table.addCell(String.valueOf(p.getQuantity()));
            table.addCell(String.valueOf(p.getPrice()));
        });

        document.add(table);
        document.close();
        System.out.println("Product pdf  generated");
    }

    private static void addTableHeaderProduct(PdfPTable table) {
        Stream.of("idProduct", "Name Product", "Quantity", "Price")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });

    }

    public static void reportOrder() throws IOException, DocumentException, SQLException {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy_MM_ddHH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream("ReportOrder" + dtf.format(now) + ".pdf"));

        document.open();

        PdfPTable table = new PdfPTable(4);
        addTableHeaderOrder(table);
        List<Orders> orders = OrderDAO.getOrderList();
        orders.forEach(o -> {
            table.addCell(String.valueOf(o.getId()));
            table.addCell(String.valueOf(o.getClientId()));
            table.addCell(String.valueOf(o.getProductId()));
            table.addCell(String.valueOf(o.getQuantity()));
        });

        document.add(table);
        document.close();
        System.out.println("Order pdf generated");
    }

    private static void addTableHeaderOrder(PdfPTable table) {
        Stream.of("idOrder", "clientId", "productId", "quantity")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });

    }
}
