package presentation;

import businessLayer.ClientBLL;
import businessLayer.OrderBLL;
import businessLayer.ProductBLL;
import model.BasicModel;
import model.Client;
import model.Orders;
import model.Product;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.logging.Logger;

public class Controller {
    protected static final Logger LOGGER = Logger.getLogger(Controller.class.getName());

    /**
     * This method identitfy the operation from command line, extract the object from cmd(if an object is specified,
     * and performs the specified operations
     *
     * @param comandLine  a string specified in command line composed from an operation
     *                   : object required to perform this operation. Report operations don`t need an object.
     * @throws Exception
     */
    public static void processCommandLine(String comandLine) throws Exception {
        LOGGER.info("Command line " + comandLine);
        Operation operation = Operation.fromString(comandLine.split(":")[0]);
        if (operation == null) {
            throw new RuntimeException("Invalid parameter " + comandLine.split(":")[0]);
        }
        if (comandLine.split(":").length > 1) {
            String objectAsString = comandLine.split(": ")[1];

            BasicModel model = buildBasicModelObject(operation, objectAsString);
            if (model instanceof Client) {
                ClientBLL clientBll = new ClientBLL();
                if (operation.equals(Operation.INSERT_CLIENT)) {

                    LOGGER.info("Insert Client " + model.toString());
                    clientBll.insertClient((Client) model);
//
                } else if (operation.equals(Operation.DELETE_CLIENT)) {

                    LOGGER.info("Delete Client " + model.toString());
                    clientBll.deleteClient((Client) model);
                }
            } else if (model instanceof Product) {

                ProductBLL productBll = new ProductBLL();
                if (operation.equals(Operation.INSERT_PRODUCT)) {

                    LOGGER.info("Insert Product " + model.toString());
                    productBll.insertProduct((Product) model);
//
                } else if (operation.equals(Operation.DELETE_PRODUCT)) {

                    LOGGER.info("Delete product " + model.toString());
                    productBll.deleteProduct((Product) model);
                }
            } else if (model instanceof Orders) {
                OrderBLL orderBll = new OrderBLL();
                if (operation.equals(Operation.ORDER)) {

                    LOGGER.info("Insert order " + model.toString());
                    orderBll.insertOrder((Orders) model);
                } else {
                    throw new RuntimeException("Counldn't determine type for " + model.toString());
                }
            }
        } else {
            LOGGER.info(operation.toString());
            if (operation.equals(Operation.REPORT_CLIENT)) {
                LOGGER.info("Report client");
                ReportGenerator.reportClient();
            } else if (operation.equals(Operation.REPORT_PRODUCT)) {
                LOGGER.info("Report product");
                ReportGenerator.reportProduct();
            } else if (operation.equals(Operation.REPORT_ORDER)) {
                LOGGER.info("Report order");
                ReportGenerator.reportOrder();
            }
        }
    }

    /**
     * @param operation   requested operation
     * @param objectAsString  string from cmd representing the object necessary to compute the operation
     *                       According to the specified operation this method extracts from the string the specific object for the input mathodand returns it
     * @param <T>
     *@return client, product or order
     * @throws NoSuchMethodException
     * @throws IllegalAccessException
     * @throws InvocationTargetException
     * @throws InstantiationException
     */
    private static <T extends BasicModel> T buildBasicModelObject(Operation operation, String objectAsString) throws
            NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        String[] objectProperties = objectAsString.trim().split(", ");
        BasicModel basicModel = new BasicModel();

        if (operation.equals(Operation.INSERT_CLIENT) || operation.equals(Operation.DELETE_CLIENT)) {
            if (objectProperties.length == 1) {
                Constructor<Client> constructor = Client.class.getConstructor(String.class);
                basicModel = constructor.newInstance(objectProperties[0]);
            } else if (objectProperties.length == 2) {
                Constructor<Client> constructor = Client.class.getConstructor(String.class, String.class);
                basicModel = constructor.newInstance(objectProperties[0], objectProperties[1]);
            } else {
                throw new RuntimeException("Invalid comandline parameter " + objectAsString);
            }

        } else if (operation.equals(Operation.INSERT_PRODUCT) || operation.equals(Operation.DELETE_PRODUCT)) {
            if (objectProperties.length == 1) {
                Constructor<Product> constructor = Product.class.getConstructor(String.class);
                basicModel = constructor.newInstance(objectProperties[0]);
            } else if (objectProperties.length == 3) {
                Constructor<Product> constructor = Product.class.getConstructor(String.class, Integer.class, Float.class);
                basicModel = constructor.newInstance(objectProperties[0], Integer.parseInt(objectProperties[1]), Float.parseFloat(objectProperties[2]));
            } else {
                throw new RuntimeException("Invalid comandline parameter " + objectAsString);
            }
        } else if (operation.equals(Operation.ORDER)) {
            Constructor<Orders> constructor = Orders.class.getConstructor(String.class, String.class, Integer.class);
            basicModel = constructor.newInstance(objectProperties[0], objectProperties[1], Integer.parseInt(objectProperties[2]));
        }
        return (T) basicModel;
    }
}
