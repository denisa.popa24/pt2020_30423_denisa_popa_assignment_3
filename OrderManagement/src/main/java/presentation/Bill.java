package presentation;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfWriter;
import model.Client;
import model.Orders;
import model.Product;

import java.io.FileOutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Denisa Popa
 * Class Bill is used for generate orders bills
 */
public class Bill {

    /**
     * @param order  to access order details
     * @param client to access client details
     * @param product to access product details
     * @throws Exception
     */
    public static void bill(Orders order, Client client, Product product) throws Exception {
        String billName = "Bill" + order.getId() + ".pdf";
        String text = "Order: " + order.toString() + " Client name: " + client.getName() +
                "\nProduct: " + product.getNameProduct() + "\nPrice per kilo: " + product.getPrice() + "\nOrdered quantity: " + order.getQuantity()
                + "\nTotal:" + order.getQuantity() * product.getPrice();
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(billName));
        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Paragraph para = new Paragraph(text, font);
        document.add(para);
        document.close();
    }

    public static void insufficientStockBill(Orders order, Client client, Product product) throws Exception {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy MM dd HH.mm.ss");
        LocalDateTime now = LocalDateTime.now();
        String billName = "BillOutOfStock" + dtf.format(now) + ".pdf";
        String text = "Order for: " + client.getName() + " is not possible. \nQuantity exceeds stock for product: " + product.getNameProduct() + "\nOnly " + product.getQuantity() + " available. \nRequested: " + order.getQuantity();
        Document document = new Document();
        PdfWriter.getInstance(document, new FileOutputStream(billName));
        document.open();
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
        Paragraph para = new Paragraph(text, font);
        document.add(para);
        document.close();
    }

}
