
import presentation.Controller;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Denisa Popa
 * The orderManagement application which process customer order for
 * a warehouse with database.
 * */

public class Main {
/**
 * This main method is used to read from file
 * commands.txt all that commands for insert, delete, update
 * in a specific table from database
 * @param args is used for get the path of this file with commands
 *             but I make also possible to introduce in main path
 *             if it cannot be accesed from args
 * @exception Throwable for FileNotFound exception or IOException
 * */
    public static void main(String[] args) throws Throwable {

        String path;
        if(args.length==0){
            path="src\\main\\java\\commands.txt";
        }
        else path=args[0];
        File file = new File(path);
        FileReader fr = new FileReader(file);
        BufferedReader buff = new BufferedReader(fr);
        List<String> commands = new ArrayList<>();
        String line;
        while ((line = buff.readLine()) != null) {
            commands.add(line);
        }
        commands.forEach(c -> {
            try {
                Controller.processCommandLine(c);
            } catch (Exception e) {
                e.printStackTrace();
            }
        });


    }

}
