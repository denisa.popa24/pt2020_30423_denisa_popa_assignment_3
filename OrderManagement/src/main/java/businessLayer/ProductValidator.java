package businessLayer;

import model.Product;

public class ProductValidator implements Validator<Product> {

    public boolean validate(Product product) {
        if (!validateName(product.getNameProduct())) return false;
        if (!validatePrice(product.getPrice())) return false;
        if (!validateQuantity(product.getQuantity())) return false;
        return true;
    }

    public boolean validateName(String productName) {
        if (productName.equals(""))
            return false;
        if (!productName.matches("[ a-zA-Z]+"))
            return false;
        return true;
    }

    public boolean validatePrice(float productPrice) {
        float price;
        try {
            price = productPrice;
            if (price < 0)
                return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    public boolean validateQuantity(int productQuantity) {
        int quant;
        try {
            quant = Integer.parseInt(String.valueOf(productQuantity));
            if (quant < 0)
                return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }


}
