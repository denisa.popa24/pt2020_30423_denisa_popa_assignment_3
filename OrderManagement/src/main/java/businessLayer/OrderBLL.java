package businessLayer;

import dataAccessLayer.OrderDAO;
import model.Orders;

/**
 * @author Denisa Popa
 * Class for bussiness logic of ORDERS using validators and data access methods
 */
public class OrderBLL {
    public void insertOrder(Orders order) throws Exception {
        OrderValidator o = new OrderValidator();
        boolean valid = o.validate(order);
        if (valid) {
            OrderDAO.insertOrder(order);
        } else {
            throw new RuntimeException("Invalid or dulplicate order " + order.toString());
        }
    }

}
