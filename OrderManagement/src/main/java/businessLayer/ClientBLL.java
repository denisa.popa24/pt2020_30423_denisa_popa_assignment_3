package businessLayer;

import dataAccessLayer.ClientDAO;
import model.Client;

/**
 * @author Denisa Popa
 * Class for bussiness logic of CLIENT using validators and data access methods
 */

public class ClientBLL {
    public ClientBLL() {
    }

    public void insertClient(Client client) {
        ClientValidator v = new ClientValidator();
        boolean valid = v.validate(client);
        if (valid) {
            ClientDAO.insert(client);
        } else {
            throw new RuntimeException("Invalid or dulplicate client name " + client.toString());
        }
    }

    public void deleteClient(Client client) {
        ClientDAO.deleteClient(client);
    }

}
