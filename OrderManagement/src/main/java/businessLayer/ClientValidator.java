package businessLayer;

import dataAccessLayer.ClientDAO;
import model.Client;

public class ClientValidator implements Validator<Client> {
    public boolean validate(Client client) {
        if (!validateName(client.getName()))
            return false;
        else return validateAddress(client.getAddress());

    }

    private boolean validateName(String name) {

        if (name.equals(""))
            return false;
        if (!name.matches("[ a-zA-Z]+"))
            return false;
        Client c = ClientDAO.findByName(name);
        return c == null;

    }

    private boolean validateAddress(String address) {

        if (address.equals(""))
            return false;
        return address.matches("[\\d+[ ](?:[A-Za-z0-9.-]+[ ]?)]+");
    }
}
