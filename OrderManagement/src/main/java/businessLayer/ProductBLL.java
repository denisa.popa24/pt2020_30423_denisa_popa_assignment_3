package businessLayer;


import dataAccessLayer.ProductDAO;
import model.Product;

/**
 * @author Denisa Popa
 * Class for bussiness logic of PRODUCT using validators and data access methods
 */
public class ProductBLL {

    public int insertProduct(Product product) throws NoSuchFieldException {
        ProductValidator p = new ProductValidator();
        boolean valid = p.validate(product);
        if (valid) {
            return ProductDAO.insertProduct(product);
        } else {
            throw new RuntimeException("Invalid or dulplicate product name " + product.toString());
        }

    }

    public void deleteProduct(Product product) {
        ProductDAO.deleteProduct(product);
    }

}
