package businessLayer;

import model.Orders;

public class OrderValidator implements Validator<Orders> {
    public boolean validate(Orders order) {

        if (!validateQuantityStock(order.getQuantity())) return false;
        return true;
    }


    public boolean validateQuantityStock(int orderQuantity) {
        int quant;
        try {
            quant = Integer.parseInt(String.valueOf(orderQuantity));
            if (quant < 0)
                return false;
        } catch (Exception e) {
            return false;
        }
        return true;
    }


}
