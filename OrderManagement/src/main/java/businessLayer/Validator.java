package businessLayer;

public interface Validator<T> {

    public boolean validate(T t);
}
